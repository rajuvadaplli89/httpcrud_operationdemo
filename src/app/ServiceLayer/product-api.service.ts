import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductAPIService {

  product_api_url='http://localhost:3000/tbl_products';
  constructor(private http:HttpClient) { }


  GetAllproducts() :Observable<any>
  {
      return this.http.get(this.product_api_url);
  }

  GetAllProductsByprice(price)
  {
      let url=this.product_api_url+'?price=' + price;
      return this.http.get(url);
  }

  GetAllProductsByProductName(pname)
  {
    debugger;
      let url=this.product_api_url+'?pname=' + pname;
      return this.http.get(url)
  }

  SaveNewProduct():Observable<any>
  {
    return this.http.post(this.product_api_url,{
      "pname":"Sansung",
      "price":"8000"
    })
  }

    UpdateRecord(id):Observable<any>
    {
       let url=this.product_api_url+'/'+id;
    return this.http.put(url,{
      "pname":"Sansung New",
      "price":"18000"
    });
  }
    DeleteRecord(id):Observable<any>
    {
      let url=this.product_api_url+'/'+id;
      return this.http.delete(url);
    
    }
  }







