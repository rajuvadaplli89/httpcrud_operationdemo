import { Component, OnInit } from '@angular/core';
import { ProductAPIService } from 'src/app/ServiceLayer/product-api.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  product_list:any
  constructor(private myservice:ProductAPIService) { }

  ngOnInit() {
    this.displayAllproducts();
  }

  displayAllproducts()
  {
    // this.myservice.GetAllproducts().subscribe(apiresponse=>{
    //   this.product_list=apiresponse;
    // });

    this.myservice.GetAllProductsByProductName('apple').subscribe(api_res=>{
      this.product_list=api_res;
    })
  }

}
